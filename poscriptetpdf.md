Au cas où la transparence poserait problème pour l'imprimeur, essayer :

1. de passer en postscript

`pdf2ps guide.pdf guide.ps`

(produit un document de plus de 110 Mo)

2. de revenir au PDF

`ps2pdf guide.ps guidebricol.pdf`

(produit un document d'environ 40 Mo)

